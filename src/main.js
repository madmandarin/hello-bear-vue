import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(VueNativeSock, 'ws://pm.tada.team/ws', {
  format: 'json',
  reconnection: true,
});

new Vue({
  render: h => h(App),
}).$mount('#app');
